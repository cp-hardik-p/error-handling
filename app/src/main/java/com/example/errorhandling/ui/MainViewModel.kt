package com.example.errorhandling.ui

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.errorhandling.model.Movie
import com.example.errorhandling.service.ApiService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val apiService: ApiService) : ViewModel() {
    var movieListResponse: List<Movie> by mutableStateOf(listOf())
    val movieState = MutableStateFlow<MovieState>(MovieState.START)
    fun getMovieList() {
        viewModelScope.launch {
            apiService.getMovies().onSuccess {
                movieListResponse = it
                movieState.emit(MovieState.SUCCESS)
            }
                .onFailure {
                    movieState.emit(MovieState.FAILURE(it.localizedMessage))
                }


        }
    }
}

sealed class MovieState {
    object START : MovieState()
    object LOADING : MovieState()
    object SUCCESS : MovieState()
    data class FAILURE(val message: String) : MovieState()
}