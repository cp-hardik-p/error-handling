package com.example.errorhandling.service

import com.example.errorhandling.model.Movie
import retrofit2.http.GET

interface ApiService {
    @GET("movielist.json")
    suspend fun getMovies(): Result<List<Movie>>
}